package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.TaskDTO;

@WebService
public interface ITaskEndpoint {

    void taskMerge(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "task") final TaskDTO taskDTO);

    @WebMethod
    void taskPersist(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "task") final TaskDTO taskDTO);

    @WebMethod
    TaskDTO findTaskById(@WebParam(name = "session") final SessionDTO sessionDTO, @WebParam(name = "id") String id);

    @WebMethod
    TaskDTO findTaskByProjectId(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "projectId") String projectId);

    @WebMethod
    TaskDTO findTaskByName(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "taskName") String taskName);

    @WebMethod
    List<TaskDTO> findAllTask(@WebParam(name = "session") final SessionDTO sessionDTO);

    @WebMethod
    List<TaskDTO> findAllTaskByOwnerId(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "ownerId") String ownerId);

    @WebMethod
    List<TaskDTO> findAllTaskByPartOfName(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "partOfName") String partOfName);

    @WebMethod
    List<TaskDTO> findAllTaskByPartOfDescription(@WebParam(name = "session") final SessionDTO sessionDTO,
	    @WebParam(name = "partOfDescription") String partOfDescription);

    @WebMethod
    void removeTaskById(@WebParam(name = "session") final SessionDTO sessionDTO, @WebParam(name = "id") String id);

    @WebMethod
    void removeAllTask(@WebParam(name = "session") final SessionDTO sessionDTO);
}
