package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.dto.SessionDTO;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    void mergeSession(@WebParam(name = "session") final SessionDTO sessionDTO);

    @WebMethod
    void persistSession(@WebParam(name = "session") final SessionDTO sessionDTO);

    @WebMethod
    SessionDTO signSession(@WebParam(name = "login") final String login,
	    @WebParam(name = "password") final String password);

    @WebMethod
    SessionDTO findById(@WebParam(name = "session") final String id);

    @WebMethod
    void removeSession(@WebParam(name = "session") final SessionDTO sessionDTO);
}
