package ru.iteco.taskmanager.repository;

import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Session;

@Repository
public interface SessionRepository extends FullEntityRepository<Session, String> {

    @NotNull
    @Query(value = "SELECT a FROM Session a WHERE a.id=:id", max = 1)
    Session findById(@QueryParam("id") @NotNull final String id);
}
