package ru.iteco.taskmanager.repository;

import java.util.List;

import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Project;

@Repository
public interface ProjectRepository extends FullEntityRepository<Project, String> {

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.id=:id", max = 1)
    Project findById(@QueryParam("id") @NotNull final String id);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.name=:name", max = 1)
    Project findByName(@QueryParam("name") @NotNull final String name);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.user.id=:userId")
    List<Project> findAllByOwnerId(@QueryParam("userId") @NotNull final String userId);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE name LIKE %:partOfName%")
    List<Project> findAllByPartOfName(@QueryParam("partOfName") @NotNull final String partOfName);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE description LIKE %:partOfDescription%")
    List<Project> findAllByPartOfDescription(@QueryParam("partOfDescription") @NotNull final String partOfDescription);
}
