package ru.iteco.taskmanager.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.NoResultException;

import lombok.Getter;
import lombok.Setter;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@Getter
@Setter
@Singleton
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService implements ITaskService {

    @Inject
    private TaskRepository taskRepository;

    public void merge(@NotNull final Task task) {
    	taskRepository.merge(task);
    }

    public void persist(@NotNull final Task task) {
    	taskRepository.persist(task);
    }

    public Task findById(@NotNull final String id) {
        try {
            return taskRepository.findById(id);
        } catch (NoResultException e) {
            return null;
        }
    }

    public Task findByProjectId(@NotNull final String projectId) {
        try {
            return taskRepository.findByProjectId(projectId);
        } catch (NoResultException e) {
            return null;
        }
    }

    public Task findByName(@NotNull final String name) {
        try {
            return taskRepository.findByName(name);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Task> findAll() {
    	return taskRepository.findAll();
    }

    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
    	return taskRepository.findAllByOwnerId(ownerId);
    }

    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
    	return taskRepository.findAllByPartOfName(partOfName);
    }

    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
    	return taskRepository.findAllByPartOfDescription(partOfDescription);
    }

    public void remove(@NotNull final String id) {
    	taskRepository.remove(findById(id));
    }

    public void removeAll() {
		for (Task task : taskRepository.findAll()) {
			taskRepository.remove(task);
		}
    }
}
