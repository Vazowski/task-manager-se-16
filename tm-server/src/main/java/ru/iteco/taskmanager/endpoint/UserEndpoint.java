package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.UserDTO;
import ru.iteco.taskmanager.util.SignatureUtil;
import ru.iteco.taskmanager.util.convert.UserDTOConvertUtil;

@Getter
@Setter
@Singleton
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @Inject
    private IUserService userService;

    @WebMethod
    public void merge(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                      @WebParam(name = "user") @NotNull UserDTO userDTO) {
        SignatureUtil.validate(sessionDTO);
        userService.merge(UserDTOConvertUtil.DTOToUser(userDTO));
    }

    @WebMethod
    public void persist(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                        @WebParam(name = "user") @NotNull UserDTO userDTO) {
        SignatureUtil.validate(sessionDTO);
        userService.persist(UserDTOConvertUtil.DTOToUser(userDTO));
    }

    @WebMethod
    @Nullable
    public UserDTO findUserById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                @WebParam(name = "id") @NotNull String id) {
        SignatureUtil.validate(sessionDTO);
        return UserDTOConvertUtil.userToDTO(userService.findById(id));
    }

    @WebMethod
    @Nullable
    public List<UserDTO> findAllUser(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
        SignatureUtil.validate(sessionDTO);
        return UserDTOConvertUtil.usersToDTO(userService.findAll());
    }
}
