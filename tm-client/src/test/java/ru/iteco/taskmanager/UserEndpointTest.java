package ru.iteco.taskmanager;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.iteco.taskmanager.api.endpoint.*;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.iteco.taskmanager.endpoint.UserEndpointService;

import java.util.List;

public class UserEndpointTest {

    private IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    private ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    private SessionDTO sessionDTO;
    private UserDTO tempUser;

    @Before
    public void connect() {
        sessionDTO = sessionEndpoint.signSession("test", "1234");
        sessionEndpoint.persistSession(sessionDTO);

        tempUser = new UserDTO();
        tempUser.setId("1");
        tempUser.setFirstName("TestUser");
        tempUser.setMiddleName("");
        tempUser.setLastName("");
        tempUser.setEmail("");
        tempUser.setLogin("TestLoginUser");
        tempUser.setPasswordHash("81dc9bdb52d04dc20036dbd8313ed055");
        tempUser.setPhone("");
        tempUser.setRoleType(RoleType.USER);
        userEndpoint.persist(sessionDTO, tempUser);
        tempUser.setId("2");
        tempUser.setFirstName("TestUser 2");
        tempUser.setLogin("TestLoginUser2");
        userEndpoint.persist(sessionDTO, tempUser);
    }

    @Test
    public void userExist() {
        @Nullable
        UserDTO testUser = new UserDTO();
        testUser.setId("1");
        @Nullable
        final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, "1");
        Assert.assertEquals(userDTO.getId(), testUser.getId());
    }

    @Test
    public void usersExists() {
        final List<UserDTO> usersList = userEndpoint.findAllUser(sessionDTO);
        Assert.assertEquals(usersList.size(), 4);
    }
}
