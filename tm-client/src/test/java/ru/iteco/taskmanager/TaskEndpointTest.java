package ru.iteco.taskmanager;

import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.iteco.taskmanager.api.endpoint.*;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.iteco.taskmanager.endpoint.TaskEndpointService;

import java.util.List;

public class TaskEndpointTest {

    private ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    private ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    private SessionDTO sessionDTO;
    private TaskDTO tempTask;

    @Before
    public void connect() {
        sessionDTO = sessionEndpoint.signSession("test", "1234");
        sessionEndpoint.persistSession(sessionDTO);

        tempTask = new TaskDTO();
        tempTask.setId("1");
        tempTask.setName("Test task");
        tempTask.setDateCreated("10.10.2020");
        tempTask.setDateBegin("10.01.2010");
        tempTask.setDateEnd("10.01.2030");
        tempTask.setDescription("This is description for test task");
        tempTask.setType("Task");
        tempTask.setOwnerId("555");
        tempTask.setReadinessStatus(ReadinessStatus.PLANNED);
        taskEndpoint.taskPersist(sessionDTO, tempTask);
        tempTask.setId("2");
        tempTask.setName("Test task for multi tasks");
        taskEndpoint.taskPersist(sessionDTO, tempTask);
    }

    @Test
    public void taskExist() {
        @Nullable
        TaskDTO testTask = new TaskDTO();
        testTask.setId("1");
        @Nullable
        final TaskDTO taskDTO = taskEndpoint.findTaskById(sessionDTO, testTask.getId());
        Assert.assertEquals(taskDTO.getId(), testTask.getId());
    }

    @Test
    public void tasksExists() {
        final List<TaskDTO> projectsList = taskEndpoint.findAllTask(sessionDTO);
        Assert.assertEquals(projectsList.size(), 2);
    }

    @After
    public void disconnect() {
        taskEndpoint.removeAllTask(sessionDTO);
    }

}
