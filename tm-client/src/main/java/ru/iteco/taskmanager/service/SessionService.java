package ru.iteco.taskmanager.service;

import javax.inject.Singleton;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.Session;

@Getter
@Setter
@Singleton
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {

    @Nullable
    private Session session = null;
}
