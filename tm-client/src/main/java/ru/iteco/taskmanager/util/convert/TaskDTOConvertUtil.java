package ru.iteco.taskmanager.util.convert;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.TaskDTO;

public class TaskDTOConvertUtil {

    @Nullable
    public static TaskDTO taskToDTO(@Nullable final Task task) {
	if (task == null)
	    return null;

	@NotNull
	final TaskDTO taskDTO = new TaskDTO();
	taskDTO.setId(task.getId());
	taskDTO.setOwnerId(task.getOwnerId());
	taskDTO.setProjectId(task.getProjectId());
	taskDTO.setName(task.getName());
	taskDTO.setDescription(task.getDescription());
	taskDTO.setDateCreated(task.getDateCreated());
	taskDTO.setDateBegin(task.getDateBegin());
	taskDTO.setDateEnd(task.getDateEnd());
	taskDTO.setType("Task");
	taskDTO.setReadinessStatus(task.getReadinessStatus());
	return taskDTO;
    }

    @Nullable
    public static List<TaskDTO> tasksToDTO(@Nullable final List<Task> listTask) {
	if (listTask == null || listTask.isEmpty())
	    return null;
	@NotNull
	final List<TaskDTO> listTasksDTO = new ArrayList<>();
	for (Task task : listTask) {
	    listTasksDTO.add(taskToDTO(task));
	}
	return listTasksDTO;
    }

    @Nullable
    public static Task DTOToTask(@Nullable final TaskDTO taskDTO) {
	if (taskDTO == null)
	    return null;

	@Nullable
	final Task task = new Task();
	task.setId(taskDTO.getId());
	task.setProjectId(taskDTO.getProjectId());
	task.setOwnerId(taskDTO.getOwnerId());
	task.setName(taskDTO.getName());
	task.setDescription(taskDTO.getDescription());
	task.setDateCreated(taskDTO.getDateCreated());
	task.setDateBegin(taskDTO.getDateBegin());
	task.setDateEnd(taskDTO.getDateEnd());
	task.setReadinessStatus(taskDTO.getReadinessStatus());
	return task;
    }

    @Nullable
    public static List<Task> DTOsToTasks(@Nullable final List<TaskDTO> listTaskDTOs) {
	if (listTaskDTOs == null || listTaskDTOs.isEmpty())
	    return null;
	@NotNull
	final List<Task> listTasks = new ArrayList<>();
	for (TaskDTO taskDTO : listTaskDTOs) {
	    listTasks.add(DTOToTask(taskDTO));
	}
	return listTasks;
    }
}
