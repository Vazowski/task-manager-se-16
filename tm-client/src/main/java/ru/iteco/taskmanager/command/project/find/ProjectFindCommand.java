package ru.iteco.taskmanager.command.project.find;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectFindCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "project-find";
    }

    @Override
    public String description() {
	return "  -  find one project";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;

	System.out.print("Name of project: ");
	@NotNull
	final String inputName = scanner.nextLine();
	@Nullable
	ProjectDTO projectDTO = projectEndpoint.findProjectByName(sessionDTO, inputName);

	if (projectDTO == null) {
		System.out.println("No project with same name");
		return;
	}

	System.out.println("ID: " + projectDTO.getId());
	System.out.println("OwnerID: " + projectDTO.getOwnerId());
	System.out.println("Name: " + projectDTO.getName());
	System.out.println("Description: " + projectDTO.getDescription());
	System.out.println("DateCreated: " + projectDTO.getDateCreated());
	System.out.println("DateBegin: " + projectDTO.getDateBegin());
	System.out.println("DateEnd: " + projectDTO.getDateEnd());
	System.out.println("Status: " + projectDTO.getReadinessStatus().toString());
    }
}
