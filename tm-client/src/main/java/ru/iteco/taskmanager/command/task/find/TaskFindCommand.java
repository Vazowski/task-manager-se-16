package ru.iteco.taskmanager.command.task.find;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Singleton
public class TaskFindCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "task-find";
    }

    @Override
    public String description() {
        return "  -  find one task in project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        System.out.print("Name of task: ");
        @NotNull final String inputName = scanner.nextLine();
        @Nullable final Task tempTask = TaskDTOConvertUtil.DTOToTask(taskEndpoint.findTaskByName(sessionDTO, inputName));

        if (tempTask == null) {
            System.out.println("No task with same name");
            return;
        }
        System.out.println("ID: " + tempTask.getId());
        System.out.println("OwnerID: " + tempTask.getOwnerId());
        System.out.println("ProjectID: " + tempTask.getProjectId());
        System.out.println("Name: " + tempTask.getName());
        System.out.println("Description: " + tempTask.getDescription());
        System.out.println("DateCreated: " + tempTask.getDateCreated());
        System.out.println("DateBegin: " + tempTask.getDateBegin());
        System.out.println("DateEnd: " + tempTask.getDateEnd());
        System.out.println("Status: " + tempTask.getReadinessStatus().toString());
    }
}
