package ru.iteco.taskmanager.command.task.remove;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Singleton
public class TaskRemoveCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "  -  remove task from project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        System.out.print("Name of task: ");
        @NotNull final String inputName = scanner.nextLine();
        @Nullable final Task task = TaskDTOConvertUtil.DTOToTask(taskEndpoint.findTaskByName(sessionDTO, inputName));
        if (task == null) {
            System.out.println("No task with same name");
            return;
        }
        if (!task.getOwnerId().equals(userDTO.getId())) {
            System.out.println("You don't have permission");
            return;
        }

        taskEndpoint.removeTaskById(sessionDTO, task.getId());
        System.out.println("Done");
    }
}
